<?php

namespace App\Http\Controllers;

use App\Lead;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $users = Lead::paginate(15);



        return view('home')->with([
            'as' => $users
        ]);
    }
}
