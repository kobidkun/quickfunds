<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function CreditCards(Request $request)
    {

        return view('pages.creditcard');
    }

    public function HomePage(Request $request)
    {

        return view('pages.homepage');
    }

    public function ContactPage(Request $request)
    {

        return view('pages.contact');
    }

    public function PersonalLoan(Request $request)
    {

        return view('pages.personalloan');
    }

    public function HomeLoan(Request $request)
    {

        return view('pages.homeloan');
    }

    public function BusinessLoan(Request $request)
    {

        return view('pages.business');
    }

    public function EduLoan(Request $request)
    {

        return view('pages.educationloan');
    }

    public function LoanProperty(Request $request)
    {

        return view('pages.loanagproperty');
    }

    public function AutoLoan(Request $request)
    {

        return view('pages.autoloan');
    }

    public function About(Request $request)
    {

        return view('pages.about');
    }

    public function ifsc(Request $request)
    {

        return view('pages.ifsc');
    }

    public function calculator(Request $request)
    {

        return view('pages.emi');
    }

}
