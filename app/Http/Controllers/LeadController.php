<?php

namespace App\Http\Controllers;

use App\Lead;
use Illuminate\Http\Request;

class LeadController extends Controller
{
    public function SaveLead(Request $request){
        $a = new Lead();
        $a->name = $request->name;
        $a->email = $request->email;
        $a->phone = $request->phone;
        $a->income = $request->income;
        $a->city = $request->city;
        $a->employment = $request->employment;
        $a->save();
        return redirect(route('home'));
    }
}
