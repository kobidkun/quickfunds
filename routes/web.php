<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('services/credit-cards', 'PageController@CreditCards')->name('credit');
Route::get('/', 'PageController@HomePage')->name('home');
Route::get('contact/', 'PageController@ContactPage')->name('contact');
Route::get('services/personal-loan', 'PageController@PersonalLoan')->name('personal');
Route::get('services/business-loan', 'PageController@BusinessLoan')->name('business');
Route::get('services/edu-loan', 'PageController@EduLoan')->name('edu');
Route::get('services/property-loan', 'PageController@LoanProperty')->name('property');
Route::get('services/auto-loan', 'PageController@AutoLoan')->name('auto');
Route::get('services/home-loan', 'PageController@HomeLoan')->name('home.loan');
Route::get('/about', 'PageController@About')->name('about');
Route::get('/ifsc', 'PageController@ifsc')->name('ifsc');
Route::get('/calculator', 'PageController@calculator')->name('calculator');


Route::post('/service', 'LeadController@SaveLead')->name('leadsave');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('homelogin');
