@extends('base')

@section('content')



    @include('components.menu');
    @include('components.slider');



    <style>

        .card-custom {
            background: #fff;
            border-radius: 2px;
            display: inline-block;
            height: 80px;

            position: relative;
            width: 330px;
            margin: 15px;
        }

        .card-1-custom {
            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            transition: all 0.3s cubic-bezier(.25,.8,.25,1);
        }

        .card-1-custom:hover {
            box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
            cursor: pointer;
        }


    </style>





    <style>

        .rate-counter-block {
            border-right: none;!important;
            padding-top: 20px;
            padding-bottom: 20px;
        }






        .rate-counter-block {


            box-shadow: 7px 6px 5px #b7b7b7;!important;
        }

        .rate-counter-block :hover{

            cursor: pointer;


        }







    </style>


    <div class="section-space80">
        <div class="container">
            <div class="row">





                <div class="col-md-4 col-sm-6 col-xs-6 ">


                    <div class="card-custom card-1-custom"  onclick="window.location='{{route('personal')}}';">

                        <div class="icon rate-icon  ">
                            <img src="/banklogo/piggi-bank.svg"
                                 alt=""
                                 class="icon-svg-1x">
                        </div>
                        <div class="rate-box ">
                            <h1 class="loan-rate" style="color: #ffffff">.</h1>
                            <small class="rate-title" >Personal Loan</small>
                        </div>

                    </div>


                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="card-custom card-1-custom"  onclick="window.location='{{route('business')}}';" >
                        <div class="icon rate-icon  ">
                            <img src="/banklogo/loan.svg"
                                                           alt="Borrow - Loan Company Website Template"
                                                           class="icon-svg-1x">
                        </div>
                        <div class="rate-box">
                            <h1 class="loan-rate" style="color: #ffffff">.</h1>
                            <small class="rate-title">Business Loan</small>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="card-custom card-1-custom" onclick="window.location='{{route('home.loan')}}';" >
                        <div class="icon rate-icon  ">
                            <img src="/banklogo/homeloan.svg"
                                                           alt="Borrow - Loan Company Website Template"
                                                           class="icon-svg-1x"></div>
                        <div class="rate-box">
                            <h1 class="loan-rate" style="color: #ffffff">.</h1>
                            <small class="rate-title">Home Loan</small>
                        </div>
                    </div>
                </div>
                {{--<div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="rate-counter-block">
                        <div class="icon rate-icon  ">
                            <img src="/icons/007-mortgage.svg"
                                                           alt="Borrow - Loan Company Website Template"
                                                           class="icon-svg-1x">
                        </div>
                        <div class="rate-box">
                            <h1 class="loan-rate">9.00%</h1>
                            <small class="rate-title">Loan Against Property</small>
                        </div>
                    </div>
                </div>--}}
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="card-custom card-1-custom" onclick="window.location='{{route('auto')}}';" >
                        <div class="icon rate-icon  ">
                            <img src="/banklogo/car.svg"
                                                           alt="Borrow - Loan Company Website Template"
                                                           class="icon-svg-1x">
                        </div>
                        <div class="rate-box">
                            <h1 class="loan-rate" style="color: #ffffff">.</h1>
                            <small class="rate-title">Auto Loan</small>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="card-custom card-1-custom" onclick="window.location='{{route('credit')}}';" >
                        <div class="icon rate-icon  ">
                            <img src="/banklogo/credit-card.svg"
                                 alt="Borrow - Loan Company Website Template"
                                 class="icon-svg-1x">
                        </div>
                        <div class="rate-box">
                            <h1 class="loan-rate" style="color: #ffffff">.</h1>
                            <small class="rate-title">Credit Card</small>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="card-custom card-1-custom" onclick="window.location='{{route('edu')}}';" >
                        <div class="icon rate-icon  ">
                            <img src="/banklogo/education.svg"
                                                           alt="Borrow - Loan Company Website Template"
                                                           class="icon-svg-1x">
                        </div>
                        <div class="rate-box">
                            <h1 class="loan-rate" style="color: #ffffff">.</h1>
                            <small class="rate-title">Education Loan</small>
                        </div>
                    </div>
                </div>



                {{--NExxt--}}



                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="card-custom card-1-custom" onclick="window.location='{{route('auto')}}';" >
                        <div class="icon rate-icon  ">
                            <img src="/banklogo/gold.png"
                                                           alt="Borrow - Loan Company Website Template"
                                                           class="icon-svg-1x">
                        </div>
                        <div class="rate-box">
                            <h1 class="loan-rate" style="color: #ffffff">.</h1>
                            <small class="rate-title">Gold Loan</small>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="card-custom card-1-custom" onclick="window.location='{{route('credit')}}';" >
                        <div class="icon rate-icon  ">
                            <img src="/banklogo/property.png"
                                 alt="Borrow - Loan Company Website Template"
                                 class="icon-svg-1x">
                        </div>
                        <div class="rate-box">
                            <h1 class="loan-rate" style="color: #ffffff">.</h1>
                            <small class="rate-title">Loan Against Property</small>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="card-custom card-1-custom" onclick="window.location='{{route('edu')}}';" >
                        <div class="icon rate-icon  ">
                            <img src="/banklogo/transfer.png"
                                                           alt="Borrow - Loan Company Website Template"
                                                           class="icon-svg-1x">
                        </div>
                        <div class="rate-box">
                            <h1 class="loan-rate" style="color: #ffffff">.</h1>
                            <small class="rate-title">Balance Transfer</small>
                        </div>
                    </div>
                </div>





            </div>
        </div>
    </div>



    <div class="bg-white section-space80">
        <div class="container">

            <div class="row">
                <div class="offset-xl-2 col-xl-8 offset-md-2 col-md-8 offset-md-2 col-md-8 col-sm-12 col-12">
                    <div class="mb100 text-center section-title">
                        <!-- section title start-->
                        <h1>How does it work?</h1>
                        <p> We have a fast and easy application process</p>
                    </div>
                    <!-- /.section title start-->
                </div>
            </div>



            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="bg-white pinside40 number-block mb30 outline">
                        <div class="circle"><span class="number">1</span></div>
                        <h3>Choose Loan Type</h3>
                        <p>Choose the type of loan you are looking for and share your details. Our team will contact you within 24 hours. .</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="bg-white pinside40 number-block mb30 outline">
                        <div class="circle"><span class="number">2</span></div>
                        <h3>Document Submission</h3>
                        <p >We will screen your profile and after due diligence, a relationship manager will be assigned to guide you through the necessary documents.</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="bg-white pinside40 number-block mb30 outline">
                        <div class="circle"><span class="number">3</span></div>
                        <h3>Get your loan</h3>
                        <p>Your application is shared with our banking partners for verification. Upon successful verification loan is disbursed to your account.</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="cta">
        <div class="container">



            <div class="row">
                <div class="offset-xl-2 col-xl-8 offset-md-2 col-md-8 offset-md-2 col-md-8 col-sm-12 col-12">
                    <div class="mb100 text-center section-title">
                        <!-- section title start-->
                        <h1 class="title-white">Why People Choose Us.</h1>
                        <p class="text-white">
                            We understand how to effectively guide you through the
                            loan or refinance process and avoid potential problems along the way.

                        </p>
                    </div>
                    <!-- /.section title start-->
                </div>
            </div>





            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="number-block text-white">
                        <div class="mb30"><i class="icon-command  icon-4x icon-white"></i></div>
                        <h3 class="text-white mb30">Dedicated Specialists</h3>
                        <p>Highly trained team of professionals to guide and answer all your queries.</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6   col-xs-12">
                    <div class="number-block text-white">
                        <div class="mb30"><i class="icon-calculator  icon-4x icon-white"></i></div>
                        <h3 class="text-white mb30">No Broker Fee</h3>
                        <p>You read it right we don’t haggle with our clients for the pie from your loan.

                            .</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6   col-xs-12">
                    <div class="number-block text-white">
                        <div class="mb30"><i class="icon-dialog  icon-4x icon-white"></i></div>
                        <h3 class="text-white mb30">Our Partners</h3>
                        <p>We are working with 10 Plus banks and NBFCs with our presence in tier 1 and tier 2 cities.

                            .</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="section-space40 bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-4 col-xs-6"><img src="/bank-logo/axis.png"
                                                             alt="Borrow - Loan Company Website Template"></div>
                <div class="col-md-2 col-sm-4 col-xs-6"><img src="/bank-logo/hfdc.png"
                                                             alt="Borrow - Loan Company Website Template"></div>
                <div class="col-md-2 col-sm-4 col-xs-6"><img src="/bank-logo/icici.png"
                                                             alt="Borrow - Loan Company Website Template"></div>
                <div class="col-md-2 col-sm-4 col-xs-6"><img src="/bank-logo/kotak.png"
                                                             alt="Borrow - Loan Company Website Template"></div>
                <div class="col-md-2 col-sm-4 col-xs-6"><img src="/bank-logo/SBI_logo2017.png"
                                                             alt="Borrow - Loan Company Website Template"></div>
                <div class="col-md-2 col-sm-4 col-xs-6"><img src="/bank-logo/union.png"
                                                             alt="Borrow - Loan Company Website Template"></div>
            </div>
        </div>
    </div>--}}
    {{--<div class="section-space80">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8">
                    <div class="mb60 text-center section-title">
                        <!-- section title start-->
                        <h1>Learn Help &amp; Guide</h1>
                        <p>Our mission is to deliver reliable, latest news and opinions.</p>
                    </div>
                    <!-- /.section title start-->
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="post-block mb30">
                        <div class="post-img">
                            <a href="blog-single.html" class="imghover"><img src="images/blog-img.jpg"
                                                                             alt="Borrow - Loan Company Website Template"
                                                                             class="img-responsive"></a>
                        </div>
                        <div class="bg-white pinside40 outline">
                            <h3><a href="blog-single.html" class="title">Couples are Happy with Buying New Home Loan</a>
                            </h3>
                            <p class="meta"><span class="meta-date">Aug 25, 2017</span><span class="meta-author">By<a
                                            href="#"> Admin</a></span></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="post-block mb30">
                        <div class="post-img">
                            <a href="blog-single.html" class="imghover"><img src="images/blog-img-1.jpg"
                                                                             alt="Borrow - Loan Company Website Template"
                                                                             class="img-responsive"></a>
                        </div>
                        <div class="bg-white pinside40 outline">
                            <h3><a href="blog-single.html" class="title">Bigger Home Still The Goal Lorem Ipsum</a></h3>
                            <p class="meta"><span class="meta-date">Aug 24, 2017</span><span class="meta-author">By<a
                                            href="#"> Admin</a></span></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="post-block mb30">
                        <div class="post-img">
                            <a href="blog-single.html" class="imghover"><img src="images/blog-img-2.jpg"
                                                                             alt="Borrow - Loan Company Website Templates"
                                                                             class="img-responsive"></a>
                        </div>
                        <div class="bg-white pinside40 outline">
                            <h3><a href="blog-single.html" class="title">Are you students looking for loan ?</a></h3>
                            <p class="meta"><span class="meta-date">Aug 24, 2017</span><span class="meta-author">By<a
                                            href="#"> Admin</a></span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}
    <div class="bg-white section-space80">
        <div class="container">


            <div class="row">
                <div class="offset-xl-2 col-xl-8 offset-md-2 col-md-8 offset-md-2 col-md-8 col-sm-12 col-12">
                    <div class="mb100 text-center section-title">
                        <!-- section title start-->
                        <h1>Our Client Testimonial.</h1>
                        <p>
                            See what our clients have to say about us

                        </p>
                    </div>
                    <!-- /.section title start-->
                </div>
            </div>




            <div class="row">
                <div class="col-md-6 col-sm-4 clearfix col-xs-12">
                    <div class="testimonial-block mb30 text-center">
                        <div class=" mb20 testimonial-img-1"><img src="{{asset('images/test/1.jpg')}}"
                                                                  alt="Anurag Singh Chauhan"
                                                                  class="img-circle"></div>
                        <div class="mb20">
                            <p class="testimonial-text"> “I loved the customer service and hassle free home loan disbursal. Truly professionals in all aspects. Thank You! Qwikfunds”</p>
                        </div>
                        <div class="testimonial-autor-box">
                            <div class="testimonial-autor">
                                <h4 class="testimonial-name-1">Prakash Dwivedi</h4>
                                <span class="testimonial-meta">Area Manager Info Edge India Ltd</span></div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-4 clearfix col-xs-12">
                    <div class="testimonial-block mb30 text-center">
                        <div class="mb20 testimonial-img-1"><img src="{{asset('/images/test/2.jpg')}}"
                                                                 alt="Anurag Singh Chauhan"
                                                                 class="img-circle"></div>
                        <div class="mb20">
                            <p class="testimonial-text">

                                The team swiftly handled and facilitated the entire process of issuing credit card. Will definitely recommend Qwikfunds

                            </p>
                        </div>
                        <div class="testimonial-autor-box">
                            <div class="testimonial-autor">
                                <h4 class="testimonial-name-1">Anurag Singh Chauhan</h4>
                                <span class="testimonial-meta">Sales Manager, Linkedin</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="section-space80">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8">
                    <div class="mb60 text-center section-title">
                        <!-- section title-->
                        <h1>We are here to help you</h1>
                        <p>Our mission is to deliver reliable, latest news and opinions.</p>
                    </div>
                    <!-- /.section title-->
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="bg-white bg-boxshadow pinside40 outline text-center mb30">
                        <div class="mb40"><i class="icon-briefcase icon-2x icon-default"></i></div>
                        <h2 class="capital-title">Branch Office</h2>
                        <p>
                            104, Opp DLF Chattarpur Farms,<br>
                            Chattarpur Mandir Road, <br>
                            New Delhi - 110074
                            <br>
                        </p>
                        <a href="{{route('contact')}}" class="btn-link">Get Appointment</a></div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="bg-white bg-boxshadow pinside40 outline text-center mb30">
                        <div class="mb40"><i class="icon-phone-call icon-2x icon-default"></i></div>
                        <h2 class="capital-title">Contact us </h2>
                        <h1 class="text-big">+91 99 33 83 7777 </h1>
                        <p>contact@qwikfunds.com</p>
                        <a href="{{route('contact')}}" class="btn-link">Contact us</a></div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="bg-white bg-boxshadow pinside40 outline text-center mb30">
                        <div class="mb40"><i class="icon-users icon-2x icon-default"></i></div>
                        <h2 class="capital-title">Talk to Advisor</h2>
                        <p>Need to loan advise? Talk to our Loan advisors.</p>
                        <a href="{{route('contact')}}" class="btn-link">Meet The Advisor</a></div>
                </div>
            </div>
        </div>
    </div>--}}


@endsection