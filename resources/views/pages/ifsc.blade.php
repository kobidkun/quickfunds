@extends('base')

@section('content')


    @include('components.menu');

    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li><a href="/">Home</a></li>
                            <li class="active">IFSC CODE</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class=" ">
        <!-- content start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper-content bg-white ">
                        <div class="about-section pinside40">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">



                                    <!-- Text input-->
                                    <div class="form-group col-md-6">
                                        <label class="control-label sr-only" for="phone">IFSC</label>
                                        <input id="ifscdata" name="income" type="text" placeholder="IFSC"
                                               class="form-control input-md" required="">
                                    </div>
                                    <!-- Button -->
                                    <div class="form-group col-md-6">

                                        <button type="submit" class="btn btn-default btn-block postifsc">Find DETAILS</button>
                                    </div>


                                    <div class="text">

                                    </div>





                                </div>


                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer')

    <script>

        $(document).ready(function(){
            $(".postifsc").click(function(){

                var code = $('#ifscdata').val();

                var URIL = 'https://ifsc.razorpay.com/'


                $.getJSON(URIL+code, function(result){

                  //  $.each(result, function(i, field){
                        $(".text").append('<br>'+result.STATE+ '<br>'+result.BRANCH+ '<br>'+result.ADDRESS+ '<br>'+result.DISTRICT+ '<br>'+result.BANK );
                  //  });
                });
            });
        });
    </script>

    @endsection