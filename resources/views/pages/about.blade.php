@extends('base')

@section('content')


    @include('components.menu');

    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">About us</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class=" ">
        <!-- content start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper-content bg-white ">
                        <div class="about-section pinside40">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <h2 class="mb30">Who We Are?</h2>
                                    <p   class="lead mb30" style="text-align: justify;!important; text-justify: inter-word;!important;">
                                        QwikFunds.com is a fintech startup with an initiative to bring loans, insurance and investment instruments under one roof.
                                    </p>
                                        <p style="text-align: justify;!important; text-justify: inter-word;!important;">
                                        We are working with 10 plus banks, NBFCs and Insurance companies with an endeavor to add more financial institutions to the portfolio. Our clients can choose from a wide spectrum of products ranging from credit cards, personal loan, home loan, business loan, vehicle loan, education loan, general insurance, life insurance, fixed deposit and mutual funds
                                    </p>

                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="mb60">
                                        <h2 class="mb30">Why QwikFunds.com?</h2>
                                        <p class="lead mb30" style="text-align: justify;!important; text-justify: inter-word;!important;" >
                                            The company is backed by ex naukri.com team (Info Edge India Ltd)  with vast and
                                            enriching experience in online domain.

                                        </p>
                                        <p style="text-align: justify; text-justify: inter-word;">
                                            Wide range of financial and investment services to choose from.
                                            Highly trained team of professionals to assist you through the entire
                                            process in choosing the right service. Do away with tedious EMI calculation,
                                            as our intuitive EMI calculator tool will help you in deciding the loan tenure
                                            or you could play around with interest rates offered to you.


                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>



                        <div class="cta-section pinside60 bg-white section-space120">
                            <div class=" ">
                                <div class="row">
                                    <div class="col-md-offset-2 col-md-8">
                                        <div class="mb60 text-center section-title">
                                            <!-- section title start-->
                                            <h1>We are here to help you</h1>
                                            <p>Our mission is to deliver reliable, latest news and opinions.</p>
                                        </div>
                                        <!-- /.section title start-->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="bg-white bg-boxshadow pinside40 outline text-center mb30">
                                            <div class="mb40"><i class="icon-calendar-3 icon-2x icon-default"></i></div>
                                            <h2 class="capital-title">Apply For Loan</h2>
                                            <p>Looking to buy a car or home loan? then apply for loan now.</p>
                                            <a href="{{route('home')}}" class="btn-link">Book Now</a> </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="bg-white bg-boxshadow pinside40 outline text-center mb30">
                                            <div class="mb40"><i class="icon-phone-call icon-2x icon-default"></i></div>
                                            <h2 class="capital-title">Call us at </h2>
                                            <h1 class="text-big">+91 99 33 83 7777 </h1>
                                            <p>contact@qwikfunds.com</p>
                                            <a href="#" class="btn-link">Contact us</a> </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="bg-white bg-boxshadow pinside40 outline text-center mb30">
                                            <div class="mb40"> <i class="icon-compass icon-2x icon-default"></i></div>
                                            <h2 class="capital-title">Reach Us</h2>
                                            <p>104, Opp DLF Chattarpur Farms,
                                                Chattarpur Mandir Road,
                                                New Delhi - 110074
                                            </p>
                                            <a href="{{route('contact')}}" class="btn-link">Map Direction</a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection