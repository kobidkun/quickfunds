<div class="slider" id="slider" style="margin-top: -20px;">
    <!-- slider -->

    <div>
        <div class="slider-img"><img src="{{asset('/serviceimages/business-loan.jpg')}}" alt="Borrow - Loan Company Website Template" class="">
            <div class="container">
                <div class="col-lg-6 col-md-12  col-sm-12 col-xs-12">
                    <div class="slider-captions">
                        <!-- slider-captions -->
                        <h1 class="slider-title">Business Loan</h1>
                        <p class="slider-text hidden-xs">Help your business grow, with collateral free Business Loans.
                        </p>
                        <a href="{{route('business')}}" class="btn btn-default hidden-xs">Apply Now</a></div>
                    <!-- /.slider-captions -->
                </div>
            </div>
        </div>
    </div>
    <div class="slider-img"><img src="{{asset('/serviceimages/personalloanbgsliger.jpg')}}"
                                 alt="Qwik Funds " class="">
        <div class="container">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="slider-captions">
                    <!-- slider-captions -->
                    <h1 class="slider-title">Personal Loan </h1>
                    <p class="slider-text hidden-xs">
                        Apply for unsecured loans.
                    </p>
                    <a href="{{route('personal')}}" class="btn btn-default hidden-xs">Apply Now</a></div>
                <!-- /.slider-captions -->
            </div>
        </div>
    </div>






  {{--  <div>
        <div class="slider-img"><img src="{{asset('/serviceimages/homeloanbg.jpg')}}" alt="Borrow - Loan Company Website Template" class="">
            <div class="container">
                <div class="col-lg-6 col-md-12  col-sm-12 col-xs-12">
                    <div class="slider-captions">
                        <!-- slider-captions -->
                        <h1 class="slider-title">Home Loan starting @ 8.35%</h1>
                        <p class="slider-text hidden-xs">
                            Why pay rent? When you can buy your own House.
                        </p>
                        <a href="{{route('home.loan')}}" class="btn btn-default hidden-xs">Apply Now</a></div>
                    <!-- /.slider-captions -->
                </div>
            </div>
        </div>
    </div>--}}

    <div>
        <div class="slider-img"><img src="{{asset('/serviceimages/carloansliderbg.jpg')}}" alt="Borrow - Loan Company Website Template" class="">
            <div class="container">
                <div class="col-lg-6 col-md-12  col-sm-12 col-xs-12">
                    <div class="slider-captions">
                        <!-- slider-captions -->
                        <h1 class="slider-title">Car Loan</h1>
                        <p class="slider-text hidden-xs">
                            Owning a Car made easy</p>
                        <a href="{{route('auto')}}" class="btn btn-default hidden-xs">Apply Now</a></div>
                    <!-- /.slider-captions -->
                </div>
            </div>
        </div>
    </div>
</div>