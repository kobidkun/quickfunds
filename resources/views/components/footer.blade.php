<div class="footer section-space80">
    <!-- footer -->
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="footer-logo">
                    <!-- Footer Logo -->
                    <img src="{{asset('/baseimages/logo-200x54.png')}}" alt="Borrow - Loan Company Website Templates"></div>
                <!-- /.Footer Logo -->
            </div>

        </div>
        <hr class="dark-line">
        <div class="row">

            <div class="col-md-4 col-sm-6 col-xs-6">
                <div class="widget-footer mt40">
                    <!-- widget footer -->
                    <ul class="listnone">
                        <li><a href="{{route('home')}}">Home</a></li>
                        <li><a href="{{route('about')}}">About Us</a></li>
                        <li><a href="/blog" target="_blank">Blog</a></li>
                        <li><a href="#" target="_blank">FAQ</a></li>
                        <li><a href="{{route('contact')}}">Contact Us</a></li>
                    </ul>
                </div>
                <!-- /.widget footer -->
            </div>
            <div class="col-md-4 col-sm-3 col-xs-4">
                <div class="widget-footer mt40">
                    <!-- widget footer -->
                    <ul class="listnone">
                        <li><a href="{{route('credit')}}">Credit Cards</a></li>
                        <li><a href="{{route('personal')}}">Personal Loan</a></li>
                        <li><a href="{{route('edu')}}">Education Loan</a></li>
                        <li><a href="{{route('business')}}">Business Loan</a></li>
                        <li><a href="{{route('home')}}">Home Loan</a></li>
                        <li><a href="{{route('auto')}}">Auto Loan </a></li>
                        {{--<li><a href="{{route('property')}}"> Loan Against Property </a></li>--}}
                        
                    </ul>
                </div>
                <!-- /.widget footer -->
            </div>
            <div class="col-md-4 col-sm-3 col-xs-4">
                <div class="widget-social mt40">
                    <!-- widget footer -->
                    <ul class="listnone">
                        <li><a href="https://www.facebook.com/QwikFunds/" target="_blank"><i class="fa fa-facebook"></i>Facebook</a>
                        </li>

                        <li><a href="https://twitter.com/QwikFunds_Tweet"><i class="fa fa-twitter"></i>Twitter</a></li>
                        <li><a href="https://www.linkedin.com/company/qwikfunds-com/"><i class="fa fa-linkedin"></i>Linked In</a></li>
                    </ul>
                </div>
                <!-- /.widget footer -->
            </div>
        </div>
    </div>
</div>
<!-- /.footer -->
<div class="tiny-footer">
    <!-- tiny footer -->
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <p>© Copyright 2018 | QwikFunds. | Developed by <a href="https://www.tecions.com/" target="_blank">Tecions</a></p>
            </div>
            <div class="col-md-6 col-sm-6 text-right col-xs-6">
                <p>Terms of use | Privacy Policy</p>
            </div>
        </div>
    </div>
</div>
<!-- /.tiny footer -->