<body class="animsition">
<div class="top-bar">
    <!-- top-bar -->
    <div class="container">
        <div class="row">
            <div class="col-md-2 hidden-xs hidden-sm">
                <p class="mail-text"></p>
            </div>
            <div class="col-md-2 hidden-xs hidden-sm">
                <p class="mail-text text-center"></p>
            </div>
            <div class="col-md-4 hidden-xs hidden-sm">

            </div>
            <div class="col-md-4 hidden-xs hidden-sm">
                <p class="mail-text text-center" style="color: #ffffff; font-weight: 500">Call us at +91 9933837777</p>
            </div>
        </div>
    </div>
</div>
<!-- /.top-bar -->


<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-6">
                <!-- logo -->
                <div class="logo">
                    <a href="/"><img src="{{asset('baseimages/logo-200x54.png')}}"
                                                     alt="Borrow - Loan Company Website Template"></a>
                </div>
            </div>
            <!-- logo -->
            <div class="col-md-9 col-sm-12 col-xs-12">
                <div id="navigation">
                    <!-- navigation start-->
                    <ul>
                        <li class="active"><a href="{{route('home')}}" class="animsition-link">Home</a>

                        </li>
                        <li><a href="#" class="animsition-link">Other Loans</a>
                            <ul>
                                <li><a href="{{route('personal')}}" title="Loan Image Listing"
                                       class="animsition-link">Gold Loan</a></li>
                                <li><a href="{{route('business')}}" title="Car Loan" class="animsition-link">
                                        Two Wheeler Loan</a>
                                </li>
                                <li><a href="{{route('home.loan')}}" title="Home Loan" class="animsition-link">
                                        Used Car Loan</a></li>

                                <li><a href="{{route('property')}}" title="Home Loan" class="animsition-link">
                                        Loan Against Property</a></li>


                                <li><a href="{{route('auto')}}" title="Education Loan" class="animsition-link">
                                        Project Finance</a></li>

                                <li><a href="{{route('edu')}}" title="Personal Loan" class="animsition-link">
                                        Loan Transfer</a></li>


                            </ul>
                        </li>












                        <li><a href="#" class="animsition-link">Tools</a>
                            <ul>
                                <li><a href="{{route('ifsc')}}" title="Loan Image Listing"
                                       class="animsition-link">IFSC </a></li>
                                <li><a href="#" title="Emi Calculator" class="animsition-link">
                                        Emi Calculator</a>
                                </li>



                            </ul>
                        </li>



                        <li><a href="{{route('contact')}}" title="Contact us" class="animsition-link">Contact us</a></li>
                    </ul>
                </div>
                <!-- /.navigation start-->
            </div>
        </div>
    </div>
</div>