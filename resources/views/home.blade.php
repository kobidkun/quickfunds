@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif




                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Income</th>
                                <th scope="col">City</th>
                                <th scope="col">Employment</th>
                            </tr>
                            </thead>
                            <tbody>



                            @foreach($as as $a)


                            <tr>
                                <th scope="row">{{$a->id}}</th>
                                <td>{{$a->name}}</td>
                                <td>{{$a->email}}</td>
                                <td>{{$a->phone}}</td>
                                <td>{{$a->income}}</td>
                                <td>{{$a->city}}</td>
                                <td>{{$a->employment}}</td>


                            </tr>

                                @endforeach

                            </tbody>
                        </table>

                        {{ $as->links() }}





                </div>
            </div>
        </div>
    </div>
</div>
@endsection
