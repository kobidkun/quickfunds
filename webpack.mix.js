let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css');


mix.scripts([
    'resources/assets/frontend/js/jquery.min.js',
    'resources/assets/frontend/js/bootstrap.min.js',
    'resources/assets/frontend/js/menumaker.js',
    'resources/assets/frontend/js/animsition.js',
    'resources/assets/frontend/js/animsition-script.js',
    'resources/assets/frontend/js/jquery.sticky.js',
    'resources/assets/frontend/js/sticky-header.js',
    'resources/assets/frontend/js/owl.carousel.min.js',
    'resources/assets/frontend/js/slider-carousel.js',
    'resources/assets/frontend/js/service-carousel.js',
    'resources/assets/frontend/js/back-to-top.js',
], 'public/js/all.min.js');


mix.styles([
    'resources/assets/frontend/css/bootstrap.min.css',
    'resources/assets/frontend/css/style.css',
    'resources/assets/frontend/css/font-awesome.min.css',
    'resources/assets/frontend/css/fontello.css',
    'resources/assets/frontend/css/animsition.min.css',
    'resources/assets/frontend/css/owl.carousel.css',
    'resources/assets/frontend/css/owl.theme.css'
], 'public/css/all.min.css');
